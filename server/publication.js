if (Meteor.isServer) {
    Meteor.startup(function () {
/*         if (graphCollection.find({"name": "demo"}).count() === 0) {
            console.log("Importing private/graph.json to db");

            var data = JSON.parse(Assets.getText("graph.json"));

            console.log(data.length);

            data.forEach(function (item, index, array) {
                graphCollection.insert(item);
            })
        } */

        if (graphCollection.find({"name": "rawdata"}).count() === 0) {
            console.log("Importing private/rawdata.json to db");

            var data = JSON.parse(Assets.getText("rawdata.json"));

            console.log(data.length);

            data.forEach(function (item, index, array) {
                graphCollection.insert(item);
            });
        }

        Meteor.publish('graph', function () {
            return graphCollection.find();
        });
    })
}
