Template.contour.rendered = function() {

    var rawdata = graphCollection.find({"name": "rawdata"}).fetch()[0];
    var wordspace = rawdata["wordspace"];
    for (var i=0;i<wordspace.length;i++){
        d3.select("#wordspace").append("option").attr("value", wordspace[i]);
    }
    var legend = Meteor.myFront.createLegend(d3.select(".legendPanel"), [0,1,2,3,4,5]);
    var smallMultiples = d3.selectAll(".small")
        .each(function () {
			var width = 250,
                height = width;
			if (this.id == "entire"){
				d3.select(this).select(".smallDIV")                
					.append("svg")
					.attr("width", "100%")
					.attr("height", "100%")
                    .attr("viewBox", "0 0 " + width + " " + height )
                    .attr("preserveAspectRatio", "xMidYMid meet");
			}
            var timeidx = this.id.split("T")[1];            
            d3.select("#T" + timeidx)
                .select(".smallDIV")
                .append("svg")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("viewBox", "0 0 " + width + " " + height )
                .attr("preserveAspectRatio", "xMidYMid meet");
        });

	Deps.autorun(function() {
		var targetsReady = Session.get("targetsReady");
		var targets = Meteor.myVar.targets;
		//var targets = ["participants", "asked", "cognitive", "studies", "questions", "participant", "answer"];
		console.log(targets)
        if (targets.length == 0){
			return 0;
		}
		
		Meteor.myFront.svgClear();
		var graphdata = Meteor.myBack.createGraph(rawdata, targets);


		console.log("creating small multiples...")
		var svg = d3.select("#entire").select("svg");
		Meteor.myFront.entireLayot(svg, graphdata);
		
		Deps.autorun(function() {
			var forceEnd = Session.get("forceEnd");
			var posNodes = Meteor.myVar.posLayout;
			if (posNodes.length == 0){
				return 0;
			}

            var nodeObj = {};
            posNodes.forEach(function (d) {
                nodeObj[d.name] = d;
            });

			graphdata.slices.forEach(function(d){
				var timeidx = d.toString();
				var nodes = [],
					sets = [];
				posNodes.forEach(function (o) {
					if (o.timeidx.indexOf(timeidx)>-1 || o.type == "target"){
						nodes.push(o);
					}
				});
				graphdata.sets.forEach(function (o) {
					if (o.timeidx == timeidx) {
						sets.push(o);
					}
				});
				var groupedSet = _.groupBy(sets, "cluster");
				for (var cluster in groupedSet) {
					if (groupedSet[cluster].length > 1) {
						var words = [];
						groupedSet[cluster].forEach(function (o) {
							words.push.apply(words, o.words)
						});
						sets.push({
							"cluster": cluster,
							"timeidx": timeidx,
							"level": "0",
							"words": words
						})
					}
				}
				Meteor.myFront.drawGraph(timeidx, nodes, sets, nodeObj);

                var node = d3.selectAll(".nodeG")
                    .on("mouseover", function(d) {
                        d3.selectAll("#"+d.name).select("circle").style("stroke-width", 6);
                        d3.selectAll("#"+d.name).select("circle").style("stroke", "orange");
                    })
                    .on("mouseout",function(d){
                        d3.selectAll("#"+d.name).select("circle").style("stroke-width", 0);
                    });

            });
		});
	});
};


