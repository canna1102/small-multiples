Meteor.myBack = {
    intersect: function (a, b) {
        var d = {};
        var results = [];
        for (var i = 0; i < b.length; i++) {
            d[b[i]] = true;
        }
        for (var j = 0; j < a.length; j++) {
            if (d[a[j]])
                results.push(a[j]);
        }
        return results;
    },

    counter: function (array) {
        var result = {};
        for (i = 0; i < array.length; ++i) {
            if (!result[array[i]])
                result[array[i]] = 0;
            ++result[array[i]];
        }
        return result
    },

    verctorize: function (keywords, wordspace) {
        var binVec = new Array(wordspace.length).fill(0);
        for (var i = 0; i < wordspace.length; i++) {
            var dim = wordspace[i];
            if (keywords.indexOf(dim) > -1) {
                binVec[i] = 1;
            }
        }
        return binVec
    },

    cosineSimilarity: function (vecA, vecB) {
        var vecDotProduct = function (vecA, vecB) {
            var product = 0;
            for (var i = 0; i < vecA.length; i++) {
                product += vecA[i] * vecB[i];
            }
            return product;
        };

        var vecMagnitude = function (vec) {
            var sum = 0;
            for (var i = 0; i < vec.length; i++) {
                sum += vec[i] * vec[i];
            }
            return Math.sqrt(sum);
        };

        return vecDotProduct(vecA, vecB) / (vecMagnitude(vecA) * vecMagnitude(vecB));
    },

    isIntersected: function (listA, listB) {
        var flag = false;
        for (var i = 0; i < listA.length; i++) {
            if (listB.indexOf(listA[i]) > -1) {
                flag = true;
            }
        }
        return flag
    },

    createGraph: function (rawdata, targets) {
        var graph;

        var nodeAll = rawdata["nodeAll"];
        var wordspace = rawdata["wordspace"];
        var timeGrouped = _.groupBy(nodeAll, "timeidx");
        var timeRange = d3.extent(rawdata.nodeAll, function (d) {
            return +d.timeidx;
        });
        var timeslices = [];
        for (var timeidx = timeRange[0]; timeidx < timeRange[1] + 1; timeidx++) {
            timeslices.push(timeidx);
        }

        //console.log("creating matchDict...")
        //var matchDict = {};
        //for (var timeidx in timeslices){
        //    if (timeslices.hasOwnProperty(timeidx)) {
        //        matchDict[timeidx] = {};
        //        matchDict[timeidx]["matchList"] = [];
        //        matchDict[timeidx]["unmatchTopic"] = [];
        //        var nodeIndex = timeGrouped[timeidx];
        //        var unmatchKey = targets.slice(0);
        //        while (unmatchKey) {
        //            var unmatchVec = Meteor.myBack.verctorize(unmatchKey, wordspace);
        //            var keyMax = [],
        //                simMax = 0;
        //
        //            for (var i = 0; i < nodeIndex.length; i++) {
        //                var node = nodeIndex[i];
        //                var keywords = node["keywords"];
        //                var matched = [];
        //                for (var j = 0; j < targets.length; j++) {
        //                    var topic = targets[j];
        //                    if (unmatchKey.indexOf(topic) == -1) {
        //                        matched.push(topic);
        //                    }
        //                }
        //                var binVec = Meteor.myBack.verctorize(keywords, wordspace);
        //                var similarity = Meteor.myBack.cosineSimilarity(binVec, unmatchVec);
        //                if (similarity > simMax) {
        //                    if (!Meteor.myBack.isIntersected(matched, keywords)) {
        //                        simMax = similarity;
        //                        keyMax = keywords;
        //                        var nodeMax = node;
        //                    }
        //                }
        //            }
        //            Array.prototype.diff = function (a) {
        //                return this.filter(function (i) {
        //                    return a.indexOf(i) < 0;
        //                });
        //            };
        //            unmatchKey = unmatchKey.diff(keyMax);
        //            if (keyMax.length > 0) {
        //                matchDict[timeidx]["matchList"].push(nodeMax)
        //            }
        //            else {
        //                matchDict[timeidx]["unmatchKey"] = unmatchKey;
        //                break
        //            }
        //        }
        //    }
        //
        //}


        console.log("creating matchDict...")
        var matchDict = {};
        for (var timeidx in timeslices) {
            if (timeslices.hasOwnProperty(timeidx)) {
                matchDict[timeidx] = {};
                matchDict[timeidx]["matchList"] = [];
                matchDict[timeidx]["unmatchTopic"] = [];
                var nodeIndex = timeGrouped[timeidx];
                var unmatchKey = targets.slice(0);
                while (unmatchKey) {
                    var keyMax = [];
                    var levelMax = 0;
                    var interMax = 0;
                    var nodeMax = undefined;
                    var matched = [];
                    for (var j = 0; j < targets.length; j++) {
                        var keyword = targets[j];
                        if (unmatchKey.indexOf(keyword) == -1) {
                            matched.push(keyword);
                        }
                    }

                    for (var i = 0; i < nodeIndex.length; i++) {
                        var node = nodeIndex[i];
                        var keywords = node.keywords;
                        var intersect = Meteor.myBack.intersect(unmatchKey, keywords);
                        if (Meteor.myBack.intersect(keywords, matched).length > 0){
                            continue
                        }
                        var condition1 = intersect.length > interMax;
                        var condition2 = (intersect.length!=0) && (intersect.length == interMax) && +node["level"]>levelMax;
                        if (condition1 || condition2){
                            interMax = intersect.length;
                            levelMax = node["level"];
                            keyMax = keywords;
                            nodeMax = node;
                        }
                    }
                    var intersect = Meteor.myBack.intersect(unmatchKey, keyMax);
                    for(var i = 0;i<intersect.length;i++){
                        var idx = unmatchKey.indexOf(intersect[i]);
                        if (idx>-1){
                            unmatchKey.splice(idx,1);
                        }
                    }
                    if (keyMax.length > 0) {
                        matchDict[timeidx]["matchList"].push(nodeMax)
                    }
                    else {
                        matchDict[timeidx]["unmatchKey"] = unmatchKey;
                        break
                    }
                }
            }
        }

        console.log(matchDict)

        console.log("creating graph...")
        var coDict = {};
        var wordsPerIdxDict = {};
        graph = {"slices": timeslices, "nodes": [], "links": [], "sets": []};
        for (var timeidx in timeslices) {
            if (timeslices.hasOwnProperty(timeidx)) {
                wordsPerIdxDict[timeidx] = [];
                var matchList = matchDict[timeidx]["matchList"];
                for (var i = 0; i < matchList.length; i++) {
                    var match = matchList[i];
                    var keywords = match["keywords"];
                    graph["sets"].push({
                        "words": keywords,
                        "level": match["level"],
                        "cluster": match["cluster"],
                        "timeidx": timeidx
                    });
                    if (wordsPerIdxDict.hasOwnProperty(timeidx)) {
                        wordsPerIdxDict[timeidx] = wordsPerIdxDict[timeidx].concat(keywords);
                    }
                    else {
                        wordsPerIdxDict[timeidx] = keywords;
                    }
                    for (var j = 0; j < keywords.length; j++) {
                        var keyword = keywords[j];
                        if (coDict.hasOwnProperty(keyword)) {
                            coDict[keyword] = coDict[keyword].concat(keywords);
                        }
                        else {
                            coDict[keyword] = keywords;
                        }
                    }
                }
            }
        }
        var allwordsGraph = Object.keys(coDict);

        for (var i = 0; i < allwordsGraph.length; i++) {
            var word = allwordsGraph[i];
            var type;
            if (targets.indexOf(word) > -1) {
                type = "target";
            }
            else {
                type = "nontarget";
            }
            var timeidx = [];
            for (var j in timeslices) {
                if (timeslices.hasOwnProperty(j)) {
                    if (wordsPerIdxDict[j].indexOf(word) > -1) {
                        timeidx.push(j);
                    }
                }
            }
            graph["nodes"].push({"name": word, "type": type, "timeidx": timeidx});

            var links_dict = Meteor.myBack.counter(coDict[word]);
            delete links_dict[word];
            for (var link in links_dict) {
                var type;
                if (targets.indexOf(word) > -1 && targets.indexOf(link) > -1) {
                    type = "target";
                }
                else {
                    type = "nontarget";
                }
                var value = links_dict[link];
                var source = allwordsGraph.indexOf(word);
                var target = allwordsGraph.indexOf(link);
                graph["links"].push({
                    "source": source,
                    "target": target,
                    "value": value,
                    "type": type,
                    "test": word + "," + link
                });
            }
        }
        return graph
    }
};