Template.controlPanel.events({
    'click #keywordAdd': function(e) {
        var keywords = $("#keywordInput").val();
        if (keywords!=""){
            $("#keywordInput").val('');
            keywords = keywords.split(",");
            for (var i=0;i<keywords.length;i++){
                var keyword = keywords[i];
                Meteor.myVar.targets.push(keyword);
                $("#targetsInput").append('<li>'+keyword+'</li>');
            }
        }
    },

    'click #keywordConfirm': function(e){
        Session.set("targetsReady", new Date().valueOf());
    },

    'click #keywordReset': function(e){
        $("#targetsInput").text("");
        Meteor.myVar.targets = [];
    }
});

Template.controlPanel.rendered = function() {
    //Deps.autorun(function(){
    //
    //});

};
