This is a project built on Meteor.js platform.

Project description:

* visualize the occurrence of keyword associations in many time slices with small multiples techniques 
* support free user selection of keyword associations, allow multiple selections input
* adopt force directed layout in d3.js to draw graph

Deployed at http://smallmultiples.meteor.com/