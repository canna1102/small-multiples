Meteor.myFront = {
	svgClear: function(){
		d3.selectAll(".small").selectAll("svg > *").remove();
        Meteor.myVar.posLayout = [];
	},
	
    myFill: function(level) {
        var color = d3.scale.category10();
        if (level == "max"){
            return color("1")
        }
        if (+level > 0) {
            var graphcolor = chroma.interpolate.bezier(['white', color("1")]);
            return graphcolor((+level + 3) / 9).hex();
        }
        else {
            return "#eee";
        }
    },

    createLegend: function(selection, data){
        var legendRectSize = 18,
            legendSpacing = 4;
        var legend = selection.selectAll(".legendItem")
                    .data(data)
                    .enter()
                    .append("g")
                    .attr('class', 'legendItem')
                    .attr('transform', function(d, i) {
                        var height = legendRectSize + legendSpacing;
                        var horz = 0;
                        var vert = i * height;
                        return 'translate(' + horz + ',' + vert + ')';
                    });

        legend.append('rect')
            .attr('width', legendRectSize)
            .attr('height', legendRectSize)
            .style('fill', function(d){
                return Meteor.myFront.myFill(+d)
            })
            .style('stroke', function(d){
                return Meteor.myFront.myFill(+d)
            });

        legend.append('text')
            .attr('x', legendRectSize + legendSpacing)
            .attr('y', legendRectSize - legendSpacing)
            .text(function(d) { return "level "+d; });
        return legend
    },

    arrangeLabels: function(svg) {
        var move = 1;
        while (move > 0) {
            move = 0;
            svg.selectAll(".graphlabel")
                .each(function () {
                    var that = this,
                        a = this.getBoundingClientRect();
                    svg.selectAll(".graphlabel")
                        .each(function () {
                            if (this != that) {
                                var b = this.getBoundingClientRect();
                                if ((Math.abs(a.left - b.left) * 2 < (a.width + b.width)) &&
                                    (Math.abs(a.top - b.top) * 2 < (a.height + b.height))) {
                                    // overlap, move labels
                                    var dx = (Math.max(0, a.right - b.left) +
                                            Math.min(0, a.left - b.right)) * 0.01,
                                        dy = (Math.max(0, a.bottom - b.top) +
                                            Math.min(0, a.top - b.bottom)) * 0.02,
                                        tt = d3.transform(d3.select(this).attr("transform")),
                                        to = d3.transform(d3.select(that).attr("transform"));
                                    move += Math.abs(dx) + Math.abs(dy);

                                    to.translate = [to.translate[0] + dx, to.translate[1] + dy];
                                    tt.translate = [tt.translate[0] - dx, tt.translate[1] - dy];
                                    d3.select(this).attr("transform", "translate(" + tt.translate + ")");
                                    d3.select(that).attr("transform", "translate(" + to.translate + ")");
                                    a = this.getBoundingClientRect();
                                }
                            }
                        });
                });
        }
    },

	entireLayot: function (svg, graphdata) {
        var width = 250,
            height = width;
		
		var nodes = [],
			links = [];

		graphdata.nodes.forEach(function (o) {
			nodes.push(o);
		});
		
		graphdata.links.forEach(function (o) {
			links.push(o);
		});

        var xdomain = d3.extent(nodes, function (d) {
            return +d.x;
        });
        var ydomain = d3.extent(nodes, function (d) {
            return +d.y;
        });
        var xscale = d3.scale.linear()
            .range([0, width])
            .domain(xdomain);
        var yscale = d3.scale.linear()
            .range([0, height])
            .domain(ydomain);
        nodes.forEach(function (o) {
            o.x = xscale(o.x);
            o.y = yscale(o.y);
        });

        var force = d3.layout.force()
            .charge(-1000)
            .linkStrength(function (d) {
                if (d.type == "target") {
                    return 0;
                } else {
                    return 1;
                }
            })
            .size([width, height])
            .gravity(nodes.length/15);

        force
            .nodes(nodes)
            .links(links)
            .start();

        var link = svg.selectAll(".link")
            .data(links)
            .enter().append("line")
            .attr("class", "link")
            .style("stroke-width", 0)
            .style("stroke-width", function(d) { return d.value; });

        var nodeG = svg.selectAll(".nodeG")
            .data(nodes)
            .enter()
            .append("g")
            .attr("class", "nodeG")
            .attr("id", function(d){
                return d.name
            });

        var node = nodeG.append("circle")
            .attr("class", "node")
            .each(function (d) {
                if (d.type == "target") {
                    d3.select(this).attr({
                        fill: Meteor.myFront.myFill("max"),
                        r: 5
                    });
                }
                else{
                    d3.select(this).attr({
                        fill: "#000",
                        r: 3
                    });
                }
            });


        nodeG.each(function (d) {
			d3.select(this)
				.append("text")
				.text(function (d) {
					return d.name;
				})
				.attr("class", "graphlabel")
				.attr("text-anchor", "middle");
        });

        force.on("tick", function (tick) {
            link.attr("x1", function (d) {
                    return d.source.x;
                })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                });

            nodeG.attr("transform", function (d) {
                return 'translate(' + [d.x, d.y] + ')';
            });
        })
        .on("end", function () {
            Meteor.myFront.arrangeLabels(svg);
			Meteor.myVar.posLayout = nodeG.data();
			Session.set("forceEnd", new Date().valueOf())
        });
    },
	
    drawGraph: function(timeidx, nodes, sets, nodeObj) {

        var svg = d3.select("#T" + timeidx)
            .select("svg");

        var setPath = function (d) {
            var nodeNames = d.words;
            var positions = nodeNames.map(function (w) {
                return [+nodeObj[w].x, +nodeObj[w].y]
            });
            if (nodeNames.length == 2) {
                var interpolation = [];
                interpolation[0] = (positions[0][0] + positions[1][0]) / 2;
                interpolation[1] = (positions[0][1] + positions[1][1]) / 2;
                positions = [positions[0], interpolation, positions[1]];
            }
            return "M" +
                d3.geom.hull(positions)
                    .join("L")
                + "Z";
        };

        var sortedSet = _.sortBy(sets, 'level');
        var set = svg.selectAll("path")
            .data(sortedSet)
            .enter()
            .append("path")
            .attr("d", setPath)
            .style("fill", function(d) {
                return Meteor.myFront.myFill(d.level)
            })
            .style("stroke", function(d) {
                return Meteor.myFront.myFill(d.level)
            })
            .style("stroke-width", 20)
            .style("stroke-linejoin", "round")
            .attr("class", "set");

        var nodeG = svg.selectAll(".nodeG")
            .data(nodes)
            .enter()
            .append("g")
			.attr("transform", function(d) {
				return 'translate(' + [d.x, d.y] + ')'
			})
            .attr("class", "nodeG")
            .attr("id", function(d){
                return d.name
            });

        var node = nodeG.append("circle")
            .attr("class", "node")
            .each(function (d) {
                if (d.type == "target") {
                    d3.select(this).attr({
                        fill: Meteor.myFront.myFill("max"),
                        r: 5,
                        visibility: "visible"
                    });
                }
                if (d.type == "nontarget" ) {
                    d3.select(this).attr({
                        fill: "#000",
                        r: 3,
                        visibility: "visible"
                    });
                }
            });


        nodeG.each(function (d) {
			d3.select(this)
				.append("text")
				.text(function (d) {
					return d.name;
				})
				.attr("class", "graphlabel")
				.attr("text-anchor", "middle");
        });

        Meteor.myFront.arrangeLabels(svg);
    }
};