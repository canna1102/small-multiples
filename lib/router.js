Router.configure({
    loadingTemplate: 'loading'
});

Router.map(function() {
    return this.route('home', {
        path: '/',
        waitOn: function() {
            return Meteor.subscribe('graph');
        },
        action: function() {
            this.render('layout');
        }
    })
});